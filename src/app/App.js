import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import ProductCreate from "./product/pages/ProductCreate";
import ProductOrder from "./product/components/ProductOrder";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/productorder" component={ProductOrder}/>
          <Route path="/productcreate" component={ProductCreate} />
          <Route path="/" component={Home}/>
        </Switch>
      </Router>
    </div>
  );
};

export default App;