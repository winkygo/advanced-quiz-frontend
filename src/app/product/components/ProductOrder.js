import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {getProductOrder} from "../actions/GetProductOrder";

class ProductOrder extends Component {
  componentDidMount() {
    this.props.getProductOrder();
  }

  render() {
    console.log(this.props.productOrderDataData);
    // const ul = [];
    // for (let productItem in this.props.productData) {
    //   ul.push(
    //     <Link to={{
    //       pathname: "/notedetails"+ this.props.productData[productItem].id,
    //       state: {productData: this.props.productData[productItem]}
    //     }} key={productItem}>
    //       <li>
    //         <h4>{this.props.productData[productItem].name} </h4>
    //       </li>
    //     </Link>
    //   );
    // }

    return (
      <div>
        {/*{ul}*/}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  productOrderDataData: state.product.productOrderDataData,
});

const mapDispatchToProps = dispatch => ({
  getProductOrder: () => dispatch(getProductOrder()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductOrder);