import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAction} from "../actions/GetAction";
import {Link} from "react-router-dom";

class Product extends Component {
  componentDidMount() {
    this.props.getAction();
  }

  render() {
    const ul = [];
    for (let productItem in this.props.productData) {
      ul.push(
        <Link to={{
          pathname: "/notedetails"+ this.props.productData[productItem].id,
          state: {productData: this.props.productData[productItem]}
        }} key={productItem}>
          <li>
            <h4>{this.props.productData[productItem].name} </h4>
          </li>
        </Link>
      );
    }

    return (
      <div>
        {ul}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  productData: state.product.productData,
});

const mapDispatchToProps = dispatch => ({
  getAction: () => dispatch(getAction()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Product);