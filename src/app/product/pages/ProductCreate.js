import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {createAction} from "../actions/CreateAction";

class ProductCreate extends Component {
  constructor(props){
    super(props);
    this.createNote = this.createNote.bind(this)
  }

  createNote(){
    this.props.createAction();
  }

  render() {
    return (
      <div>
        <input value="test1"/>
        <textarea value="这是测试数据呀">
        </textarea>
        <button onClick={this.createNote}>创建</button>
        <Link to="/">
          <button>取消</button>
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product.productData,
});

const mapDispatchToProps = dispatch => ({
  createAction: () => dispatch(createAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCreate);
