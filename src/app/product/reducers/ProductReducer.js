const initialValue = {
  productData: {}
};
export default (state = initialValue,action)=>{
  switch (action.type) {
    case "GET_PRODUCTS":
      return {
        ...state,
        productData: action.productData
      };
    default:
      return state

  }
}