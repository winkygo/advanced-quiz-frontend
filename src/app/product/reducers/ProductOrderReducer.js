const initialValue = {
  productOrderData: {}
};
export default (state = initialValue,action)=>{
  switch (action.type) {
    case "GET_PRODUCTORDERS":
      return {
        ...state,
        productOrderDataData: action.productOrderDataData
      };
    default:
      return state
  }
}