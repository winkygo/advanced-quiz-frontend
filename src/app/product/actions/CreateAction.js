export const createAction = () => (dispatch) => {
  fetch('http://localhost:8080/api/product', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
    body: JSON.stringify({
      name: "cola",
      price: 2,
      quantity: 1,
      unit: "pin",
    })
  }).then()
    .catch(() => {
    console.log("Fetch error");
  })
};