export const getProductOrder = () => (dispatch) => {
  fetch("http://localhost:8080/api/product/order")
    .then(response=>response.json())
    .then(result =>{
      dispatch({
        type:"GET_PRODUCTORDERS",
        productOrderDataData:result
      });
    }).catch(()=>{
    console.log("error");
  })
};
