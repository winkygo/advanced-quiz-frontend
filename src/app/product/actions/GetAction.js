export const getAction = () => (dispatch) => {
  fetch("http://localhost:8080/api/product")
    .then(response=>response.json())
    .then(result =>{
      dispatch({
        type:"GET_PRODUCTS",
        productData:result
      });
    }).catch(()=>{
    console.log("error");
  })
};
