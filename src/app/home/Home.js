import React from 'react';
import {Link} from "react-router-dom";
import Product from "../product/components/Product";
import ProductOrder from "../product/components/ProductOrder";

const Home = () => {
  return (
    <div>
      <ul>
        <li>
          <Link to='/productcreate'>添加商品</Link>
        </li>
        <li>
          <Link to='/productorder'>订单</Link>
        </li>
        <Product />
      </ul>
    </div>
  )
};

export default Home;