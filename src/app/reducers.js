import { combineReducers } from 'redux';
import ProductReducer from "./product/reducers/ProductReducer";
import ProductOrderReducer from "./product/reducers/ProductOrderReducer";
export default combineReducers({
  product: ProductReducer,
  productOrder: ProductOrderReducer
});